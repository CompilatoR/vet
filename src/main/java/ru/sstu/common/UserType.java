package ru.sstu.common;

public enum UserType {

  CUSTOMER, VET, ADMIN, ANONYMOUS
}
