package ru.sstu.common.dto;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VisitDetailsResponse {

  private LocalDate date;
  private String anamnesis;
  private String epicrisis;
}
