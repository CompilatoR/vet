package ru.sstu.common.dto;

import java.util.Map;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContentDetails {

  private Map<String, Object> content;
}
