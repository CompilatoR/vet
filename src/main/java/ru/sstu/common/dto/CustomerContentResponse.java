package ru.sstu.common.dto;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerContentResponse {

  private String username;
  private String email;
  private Set<PetDetailsResponse> pets;

}
