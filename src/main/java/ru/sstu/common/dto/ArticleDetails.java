package ru.sstu.common.dto;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class ArticleDetails {

  private Integer id;
  private LocalDate publishingDate;
  private String topic;
  private String content;

}
