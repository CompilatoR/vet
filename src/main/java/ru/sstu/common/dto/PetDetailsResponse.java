package ru.sstu.common.dto;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PetDetailsResponse {

  private String moniker;
  private String animal;
  private int age;
  private String breed;
  private Set<VisitDetailsResponse> visits;
  private Set<VaccinationDetailsResponse> vaccinations;

}
