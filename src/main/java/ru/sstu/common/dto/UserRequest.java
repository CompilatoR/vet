package ru.sstu.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.sstu.common.UserType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

  private String username;
  private UserType userType;
}
