package ru.sstu.common.model;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PetModel {

  private String name;
  private int age;
  private List<VaccinationModel> vaccinations;
}
