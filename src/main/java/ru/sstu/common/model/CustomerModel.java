package ru.sstu.common.model;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerModel extends AbstractProfileModel {

  private List<PetModel> pets;
}
