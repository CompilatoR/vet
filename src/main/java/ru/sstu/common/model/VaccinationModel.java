package ru.sstu.common.model;

import java.time.LocalDate;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VaccinationModel {

  private LocalDate date;
  private String disease;
  private String medicine;
}
