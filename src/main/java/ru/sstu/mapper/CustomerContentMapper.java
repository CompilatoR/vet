package ru.sstu.mapper;

import org.mapstruct.Mapper;
import ru.sstu.common.dto.CustomerContentDetails;
import ru.sstu.common.dto.CustomerContentResponse;
import ru.sstu.repository.entity.ProfileEntity;

@Mapper(componentModel = "spring")
public interface CustomerContentMapper extends GenericContentMapper<CustomerContentDetails, CustomerContentResponse, ProfileEntity> {

}
