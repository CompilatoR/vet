package ru.sstu.mapper;

public interface GenericContentMapper<D, O, E> {

  O detailsToResponse(D details);

  D entityToDetails(E entity);

}
