package ru.sstu.mapper;

public interface GenericMapper<I, D, O> {

  D requestToDetails(I request);
  O detailsToResponse(D details);
}
