package ru.sstu.mapper;

import org.mapstruct.Mapper;
import ru.sstu.common.dto.ArticleDetails;
import ru.sstu.common.dto.ArticleRequest;
import ru.sstu.common.dto.ArticleResponse;
import ru.sstu.repository.entity.Article;

@Mapper(componentModel = "spring")
public interface ArticleMapper /*extends GenericMapper<ArticleRequest, ArticleDetails, Article, ArticleResponse>*/ {

}
