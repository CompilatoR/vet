package ru.sstu.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.sstu.repository.entity.ProfileEntity;

public interface ProfileRepository extends JpaRepository<ProfileEntity, Integer> {

  Optional<ProfileEntity> findByUsername(String username);
}
