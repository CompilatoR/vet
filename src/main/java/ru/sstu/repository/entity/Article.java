package ru.sstu.repository.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "KP_ARTICLES")
public class Article implements Serializable {

  @Id
  private Integer id;
  private LocalDate publishingDate;
  private String topic;
  private String content;

  @Override
  public String toString() {
    return "Article{" +
        "id=" + id +
        ", publishingDate=" + publishingDate +
        ", topic='" + topic + '\'' +
        '}';
  }
}
