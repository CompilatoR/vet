package ru.sstu.repository.entity;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Data
@Entity
@Table(name = "PETS")
@Builder
public class PetEntity {

  @Id
  private int id;
  @ManyToOne()
  @JoinColumn/*(referencedColumnName = "id")*/
  //@OneToMany(targetEntity = ProfileEntity.class, mappedBy = "pets", cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.EAGER)
  private ProfileEntity profileEntity;
  private String moniker;
  private int age;
  private String animal;
  private String breed;
  @OneToMany(mappedBy = "pet")
  @Cascade(CascadeType.SAVE_UPDATE)
  private Set<VisitEntity> visits;
  @OneToMany(mappedBy = "pet")
  @Cascade(CascadeType.SAVE_UPDATE)
  private Set<VaccinationEntity> vaccinations;

}
