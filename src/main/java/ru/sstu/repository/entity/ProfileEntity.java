package ru.sstu.repository.entity;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name = "PROFILES")
@Builder
public class ProfileEntity {

  @Id
  private int id;
  private String username;
  private String email;
  @OneToMany(mappedBy = "profileEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  //@Cascade(CascadeType.ALL)
  /*@ManyToOne()
  @JoinColumn(name="pets", referencedColumnName = "pets", insertable = false, updatable = false)*/
  private Set<PetEntity> pets;
}
