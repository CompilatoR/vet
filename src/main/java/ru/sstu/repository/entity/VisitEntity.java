package ru.sstu.repository.entity;

import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name = "VISITS")
@Builder
public class VisitEntity {

  @Id
  private int id;
  @ManyToOne()
  @JoinColumn(referencedColumnName = "id", insertable = false, updatable = false)
  private PetEntity pet;
  private LocalDate date;
  private String anamnesis;
  private String epicrisis;

}
