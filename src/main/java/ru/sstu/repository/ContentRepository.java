package ru.sstu.repository;

import ru.sstu.repository.entity.Article;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ContentRepository extends CrudRepository<Article, Integer> {

  List<Article> findByOrderByPublishingDateDesc();

}
