package ru.sstu.web.controller;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sstu.common.UserType;
import ru.sstu.common.dto.MeResponse;
import ru.sstu.util.AuthUtils;

@RestController
@RequestMapping(value = "/me")
public class MeController {

  @Autowired
  private AuthUtils authUtils;

  @GetMapping
  public MeResponse me() {

    Pair<String, UserType> userData = authUtils.getCurrentUsernameAndType();

    return MeResponse.builder()
        .username(userData.getLeft())
        .authority(userData.getRight().name())
        .build();
  }

}
