package ru.sstu.web.controller;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sstu.common.dto.ContentDetails;
import ru.sstu.util.AuthUtils;
import ru.sstu.web.content.ContentHandler;

@Controller
public class VetController {

  private static final String PAGENAME = "vet";

  @Autowired
  private AuthUtils authUtils;
  @Autowired
  private ContentHandler contentHandler;

  @RequestMapping("/" + PAGENAME)
  public String userPage(Map<String, Object> model, Authentication authentication) {
    ContentDetails contentDetails = contentHandler.getContentDetails(
        authentication.getName(),
        authUtils.getUserType(authentication));
    model.putAll(contentDetails.getContent());
    return PAGENAME;
  }

}
