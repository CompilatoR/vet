package ru.sstu.web.controller;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sstu.common.UserType;
import ru.sstu.common.dto.CustomerContentDetails;
import ru.sstu.common.dto.CustomerContentResponse;
import ru.sstu.mapper.CustomerContentMapper;
import ru.sstu.util.AuthUtils;
import ru.sstu.web.content.CustomerContentHandler;

@RestController
public class CustomerController {

  private static final String ENDPOINT = "/customer";

  @Autowired
  private AuthUtils authUtils;
  @Autowired
  private CustomerContentHandler contentHandler;
  @Autowired
  private CustomerContentMapper mapper;

  @RequestMapping(ENDPOINT)
  public CustomerContentResponse customerContent() {
    Pair<String, UserType> userData = authUtils.getCurrentUsernameAndType();
    CustomerContentDetails customerContentDetails = contentHandler.getContent(userData.getLeft());
    return mapper.detailsToResponse(customerContentDetails);
  }

}
