package ru.sstu.web.controller;

import java.util.Map;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sstu.util.AuthUtils;
import ru.sstu.web.content.ContentService;

@Slf4j
@Controller
public class RootPageController {

  @Autowired
  private AuthUtils authUtils;
  @Autowired
  private DataSource dataSource;
  @Autowired
  private ContentService contentService;


  @RequestMapping("/")
  public String mainPage(Map<String, Object> model) {
    putArticlesToModel(model);
    //putUsernameToModel(model);
    Pair<String, String> userEndpoint = authUtils.getUserEndpoint();
    if (userEndpoint != null) {
      model.put("username", userEndpoint.getLeft());
      model.put("endpoint", userEndpoint.getRight());
    }
    return "mainPage";
  }

  private void putArticlesToModel(Map<String, Object> model) {
    //model.put(Constants.ARTICLES, contentService.getArticles());
  }

  /*private void putUsernameToModel(Map<String, Object> model) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      Object principal = authentication.getPrincipal();
      log.debug("putUsernameToModel. Principal = {}", principal);
      if (authUtils.isNotAnonymous(principal)) {
        String username = authUtils.getUsername(principal);
        model.put(Constants.USERNAME, username);
      }
    }
  }*/
}