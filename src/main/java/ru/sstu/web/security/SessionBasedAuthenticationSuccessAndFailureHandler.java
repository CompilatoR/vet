package ru.sstu.web.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

public class SessionBasedAuthenticationSuccessAndFailureHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationFailureHandler {

  private RequestCache requestCache = new HttpSessionRequestCache();

  @Override
  public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication)
      throws IOException, ServletException {

    SavedRequest savedRequest
        = requestCache.getRequest(httpServletRequest, httpServletResponse);
    if (savedRequest == null) {
      clearAuthenticationAttributes(httpServletRequest);
      return;
    }

    String targetUrlParam = getTargetUrlParameter();
    if (isAlwaysUseDefaultTargetUrl()
        || (targetUrlParam != null
        && StringUtils.hasText(httpServletRequest.getParameter(targetUrlParam)))) {
      requestCache.removeRequest(httpServletRequest, httpServletResponse);
      clearAuthenticationAttributes(httpServletRequest);
      return;
    }

    User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    //httpServletResponse.setStatus(HttpServletResponse.SC_OK);
    httpServletResponse.addCookie(new Cookie("username", authUser.getUsername()));
    httpServletResponse.addCookie(new Cookie("authority", authentication.getAuthorities().stream().findFirst().get().getAuthority()));

    clearAuthenticationAttributes(httpServletRequest);
  }

  @Override
  public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e)
      throws IOException, ServletException {
    httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
  }
}
