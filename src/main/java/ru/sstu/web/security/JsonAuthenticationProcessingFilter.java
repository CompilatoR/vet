package ru.sstu.web.security;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JsonAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter {

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    AuthData authData = parseRequest(req);
    if (authData != null) {
      HttpServletRequest request = (HttpServletRequest) req;
      request.setAttribute(SPRING_SECURITY_FORM_USERNAME_KEY, authData.username);
      request.setAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY, authData.password);
    }
    super.doFilter(req, res, chain);
  }

  @Override
  protected String obtainPassword(HttpServletRequest request) {
    return (String) request.getAttribute(SPRING_SECURITY_FORM_USERNAME_KEY);
  }

  @Override
  protected String obtainUsername(HttpServletRequest request) {
    return (String) request.getAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY);
  }

  private class AuthData {

    String username;
    String password;
  }

  private AuthData parseRequest(ServletRequest request) {
    AuthData authData;
    try {
      StringBuilder buffer = new StringBuilder();
      BufferedReader reader = request.getReader();

      String line;
      while ((line = reader.readLine()) != null) {
        buffer.append(line);
      }

      Gson gson = new Gson();
      authData = gson.fromJson(buffer.toString(), AuthData.class);

    } catch (IOException e) {
      throw new NonParseableAuthenticationDataException();
    }

    return authData;
  }

  /*@Override
  public void setAuthenticationManager(AuthenticationManager authenticationManager) {
    super.setAuthenticationManager(authenticationManager);
  }*/
}
