package ru.sstu.web.security;

import org.springframework.security.core.AuthenticationException;

public class NonParseableAuthenticationDataException extends AuthenticationException {

  public NonParseableAuthenticationDataException() {
    super("Failed to parse input");
  }
}
