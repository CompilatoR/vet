package ru.sstu.web.content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sstu.common.UserType;
import ru.sstu.common.dto.ContentDetails;

@Component
public class ContentHandler {

  @Autowired
  private ContentService contentService;

  public ContentDetails getContentDetails(String username, UserType userType) {
    return contentService.getContent(username, userType);
  }

}
