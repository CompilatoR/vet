package ru.sstu.web.content;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sstu.common.UserType;
import ru.sstu.common.dto.ContentDetails;
import ru.sstu.common.model.CustomerModel;
import ru.sstu.common.model.PetModel;
import ru.sstu.common.model.VaccinationModel;
import ru.sstu.repository.ContentRepository;

@Service
public class ContentService {

  private static final String PAGE_POSTFIX = "Page";

  @Autowired
  private ContentRepository contentRepository;

  ContentDetails getContent(String username, UserType userType) {

    List<VaccinationModel> vaccinations = new ArrayList<>();
    vaccinations.add(VaccinationModel.builder()
        .date(LocalDate.of(2018, 01, 01))
        .medicine("Imovax")
        .disease("Rabies")
        .build());

    List<PetModel> pets = new ArrayList<>();
    pets.add(PetModel.builder()
        .name("Sharik")
        .age(3)
        .vaccinations(vaccinations)
        .build());
    pets.add(PetModel.builder()
        .name("Bobik")
        .age(2)
        .vaccinations(vaccinations)
        .build());

    CustomerModel customerModel = CustomerModel.builder()
        .pets(pets)
        .build();

    Map<String, Object> model = new HashMap<>();
    model.put("username", username);
    model.put("pets", pets);
    return ContentDetails.builder()
        .content(model)
        .build();
  }

}
