package ru.sstu.web.content;

import com.google.common.collect.ImmutableSet;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sstu.common.dto.CustomerContentDetails;
import ru.sstu.mapper.CustomerContentMapper;
import ru.sstu.repository.ProfileRepository;
import ru.sstu.repository.entity.PetEntity;
import ru.sstu.repository.entity.ProfileEntity;
import ru.sstu.repository.entity.VaccinationEntity;
import ru.sstu.repository.entity.VisitEntity;

@Component
public class CustomerContentHandler {

  @Autowired
  private ProfileRepository profileRepository;
  @Autowired
  private CustomerContentMapper mapper;

  public CustomerContentDetails getContent(String username) {
    Optional<ProfileEntity> profile = Optional.empty();//profileRepository.findByUsername(username); // TODO Cascade save doesn't work blyat
    ProfileEntity profileEntity = ProfileEntity.builder()
        .username(username)
        .email("compilator@yandex.ru")
        .pets(ImmutableSet.of(PetEntity.builder()
            .moniker("Sharik")
            .age(2017)
            .animal("Dog")
            .breed("German Shepherd")
            .visits(ImmutableSet.of(VisitEntity.builder()
                    .id(0)
                    .date(LocalDate.of(2019, 5, 30))
                    .anamnesis("Got cold")
                    .epicrisis("Get well")
                    .build(),
                VisitEntity.builder()
                    .id(1)
                    .date(LocalDate.of(2019, 1, 12))
                    .anamnesis("Feel bad")
                    .epicrisis("Take care")
                    .build()))
            .vaccinations(ImmutableSet.of(
                VaccinationEntity.builder()
                    .date(LocalDate.of(2018, 04, 5))
                    .disease("Rabies")
                    .medicine("Imovax")
                    .build()
            ))
            .build()))
        .build();
    //ProfileEntity saved = profileRepository.saveAndFlush(profileEntity);
    if (!profile.isPresent()) {
      ///System.out.println("saved");
    }
    return mapper.entityToDetails(profileEntity);
  }

}
