package ru.sstu;

import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.sstu.web.security.RestAuthenticationEntryPoint;
import ru.sstu.web.security.SessionBasedAuthenticationSuccessAndFailureHandler;

@Slf4j
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private DataSource dataSource;
  @Autowired
  private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

  /*@Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
        .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
        .antMatchers("/").permitAll()
        .antMatchers("/admin/*").hasRole("ADMIN")
        .antMatchers("/customer").hasRole("CUSTOMER")
        .antMatchers("/vet").hasRole("VET")
        .antMatchers("/h2/*").permitAll()
        .anyRequest().authenticated()
        .and()
        .csrf().disable()//.ignoringAntMatchers("/h2/*")
        //.and()
        .headers()
        .frameOptions()
        .sameOrigin()
        .and()
        .formLogin()
        .loginPage("/login")
        .failureUrl("/login?error")
        .permitAll()
        .and()
        .logout()
        .permitAll();
  }*/ // TODO This is for Thymeleaf-based application. Use either this or config below(for rest-based application)

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    SessionBasedAuthenticationSuccessAndFailureHandler authHandler = new SessionBasedAuthenticationSuccessAndFailureHandler();
    http
        .csrf().disable()
        .exceptionHandling()
        .authenticationEntryPoint(restAuthenticationEntryPoint)
        .and()
        .authorizeRequests()
        .antMatchers("/api/user", "/me").authenticated()
        .antMatchers("/api/admin").hasRole("ADMIN")
        .antMatchers("/h2/*").hasRole("ADMIN")
        .and()
        .formLogin()
        .successHandler(authHandler)
        .failureHandler(authHandler)
        .and()
        .logout();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    //auth.jdbcAuthentication().dataSource(dataSource); // TODO This is for jdbc authentication
    auth.inMemoryAuthentication()
        .withUser("user").password(encoder().encode("user")).roles("CUSTOMER")
        .and()
        .withUser("vet").password(encoder().encode("vet")).roles("VET")
        .and()
        .withUser("admin").password(encoder().encode("admin")).roles("ADMIN");
  }


  @Bean
  public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder();
  }
}