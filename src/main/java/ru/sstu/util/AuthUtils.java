package ru.sstu.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ru.sstu.common.UserType;

@Slf4j
@Component
public class AuthUtils {

  private static final String ANONYMOUS_USER = "anonymousUser";

  public String getUsername(Object principal) {
    String username;
    if (isAnonymous(principal)) {
      username = ANONYMOUS_USER;
    } else if (principal instanceof UserDetails) {
      username = ((UserDetails) principal).getUsername();
    } else {
      username = principal.toString();
    }
    return username;
  }

  public boolean isAnonymous(Object principal) {
    boolean isAnonymous = false;
    if (principal instanceof UserDetails) {
      isAnonymous = false;
    } else if (ANONYMOUS_USER.equalsIgnoreCase(principal.toString())) {
      isAnonymous = true;
    }
    return isAnonymous;
  }

  public boolean isNotAnonymous(Object principal) {
    return !isAnonymous(principal);
  }

  private UserType getUserType(GrantedAuthority grantedAuthority) {
    String authority = grantedAuthority.getAuthority().toUpperCase();
    switch (authority) {
      case "ROLE_CUSTOMER":
        return UserType.CUSTOMER;
      case "ROLE_VET":
        return UserType.VET;
      case "ROLE_ADMIN":
        return UserType.ADMIN;
      case "ROLE_ANONYMOUS":
        return UserType.ANONYMOUS;
      default:
        throw new IllegalStateException("Unknown authority " + authority);
    }
  }

  private Authentication getAuthentication() {
    return SecurityContextHolder.getContext().getAuthentication();
  }

  public Pair<String, String> getUserEndpoint() {
    Authentication authentication = getAuthentication();
    UserType userType = getUserType(authentication);
    if (!UserType.ANONYMOUS.equals(userType)) {
      String username = getUsername(authentication.getPrincipal());
      return Pair.of(username, userType.name().toLowerCase());
    }
    return null;
  }

  public UserType getUserType(Authentication authentication) {

    if (authentication == null) {
      log.warn("Authentication data is null. Trying to get from security context.");
      authentication = getAuthentication();
      if (authentication == null) {
        throw new IllegalStateException("Cannot identify user");
      }
    }

    GrantedAuthority authority = authentication.getAuthorities()
        .stream()
        .findFirst()
        .orElseThrow(() -> new IllegalStateException("User authorities is not defined"));

    return getUserType(authority);

  }

  public Pair<String, UserType> getCurrentUsernameAndType() {
    Authentication authentication = getAuthentication();
    UserType userType = getUserType(authentication);
    if (!UserType.ANONYMOUS.equals(userType)) {
      String username = getUsername(authentication.getPrincipal());
      return Pair.of(username, userType);
    }
    return null;
  }
}
