package ru.sstu.admin.controller;

import ru.sstu.mapper.ArticleMapper;
import ru.sstu.web.content.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/admin/article")
public class ContentAdminController {

  @Autowired
  private ContentService contentService;
  @Autowired
  private ArticleMapper mapper;

  /*@GetMapping(value = "/{articleId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Article getById(int articleId) {
    return contentService.getArticleById(articleId)
        .orElse(null);
  }

  @PostMapping
  public void create(ArticleRequest request) {
    contentService.upsertArticle(mapper.requestToEntity(request));
  }

  @PutMapping
  public void update(ArticleRequest request) {
    contentService.upsertArticle(mapper.requestToEntity(request));
  }

  @DeleteMapping("/{articleId}")
  public void delete(@PathVariable int articleId) {
    contentService.deleteArticle(articleId);
  }*/

}
