package ru.sstu.admin.controller;

import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sstu.admin.dto.InternalUserDetails;
import ru.sstu.admin.dto.UserAdminRequest;
import ru.sstu.admin.dto.UserAdminResponse;
import ru.sstu.admin.handler.SecurityAdminHandler;

@RestController
@RequestMapping(value = "/admin/user")
public class SecurityAdminController {

  @Autowired
  private SecurityAdminHandler handler;

  @PostMapping
  public UserAdminResponse createUser(@RequestBody UserAdminRequest request) {
    if (handler.userExists(request.getUsername())) {
      return UserAdminResponse.builder()
          .result("User exists")
          .build();
    } else {
      handler.createUser(InternalUserDetails.builder()
          .username(request.getUsername())
          .password("{noop}" + request.getPassword())
          .authorities(Collections.singleton(
              new SimpleGrantedAuthority("ROLE_" + request.getUserType())))
          .isEnabled(true)
          .isAccountNonExpired(true)
          .isAccountNonLocked(true)
          .isCredentialsNonExpired(true)
          .build());
      if (handler.userExists(request.getUsername())) {
        return UserAdminResponse.builder()
            .result("User created")
            .build();
      }
    }
    return UserAdminResponse.builder()
        .result("Creation failed")
        .build();
  }

  @GetMapping(value = "/{username}")
  public UserAdminResponse getUser(@PathVariable String username) {
    return UserAdminResponse.builder()
        .result("" + handler.userExists(username))
        .build();
  }

  @DeleteMapping(value = "/{username}")
  public void deleteUser(@PathVariable String username) {
    handler.deleteUser(username);
  }

}
