package ru.sstu.admin.dto;

import java.util.Set;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Data
@Builder
public class InternalUserDetails implements UserDetails {

  private Set<GrantedAuthority> authorities;
  private String password;
  private String username;
  private boolean isAccountNonExpired;
  private boolean isAccountNonLocked;
  private boolean isCredentialsNonExpired;
  private boolean isEnabled;
}
