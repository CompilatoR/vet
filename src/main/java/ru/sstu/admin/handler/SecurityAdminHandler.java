package ru.sstu.admin.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;
import ru.sstu.admin.dto.InternalUserDetails;

@Component
public class SecurityAdminHandler {

  @Autowired
  private UserDetailsManager userDetailsManager;

  public void createUser(InternalUserDetails userDetails) {
    userDetailsManager.createUser(userDetails);
  }

  public void updateUser(InternalUserDetails userDetails) {
    userDetailsManager.updateUser(userDetails);
  }

  public boolean userExists(String username) {
    return userDetailsManager.userExists(username);
  }

  public void deleteUser(String username) {
    userDetailsManager.deleteUser(username);
  }
}
